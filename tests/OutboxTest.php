<?php

namespace ActivityPhp\Tests;

use ActivityPhp\Outbox;
use ActivityPhp\Vocabulary\Actor;
use ActivityPhp\Vocabulary\Document\Video;
use PHPUnit\Framework\TestCase;

class OutboxTest extends TestCase {
    public function testWithSampleActivity() {

        $outbox = new Outbox();

        $video = new Video();
        $video->setContent('Toto is happy')
            ->setPublished(new \DateTime());

        $actor = new Actor();
        $actor->setName('Toto');

        var_dump($outbox->post($video, $actor, 'https://prefix/id/'));
    }
}