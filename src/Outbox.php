<?php

namespace ActivityPhp;

use ActivityPhp\Vocabulary\Activity;
use ActivityPhp\Vocabulary\Actor;
use ActivityPhp\Vocabulary\Object;
use Doctrine\Common\Annotations\AnnotationRegistry;
use JMS\Serializer\SerializerBuilder;

class Outbox
{
    public function post(Object $object, Actor $self, string $idPrefix): string
    {
        AnnotationRegistry::registerLoader('class_exists');

        // Creating unique id for object
        $object->setId($idPrefix . uniqid('', true));

        if (!($object instanceof Activity)) {
            $activity = new Activity\Create();
            $activity
                ->setObject($object)
                ->setId($idPrefix . uniqid('', true))
                ->setActor($self)
            ;
            if ($to = $object->getTo()) {
                $activity->setTo($to);
            }
            $object = $activity;
        }

        $serializer = SerializerBuilder::create()->build();
        return $serializer->serialize($object, 'json');
    }
}