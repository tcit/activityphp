<?php

namespace ActivityPhp\Tools\Serializer\Jms\Visitor;

use JMS\Serializer\GenericDeserializationVisitor;

class ArrayDeserialize extends GenericDeserializationVisitor
{
    public function decode($str)
    {
        return $str;
    }

    public function getResult()
    {
        return null;
    }
}