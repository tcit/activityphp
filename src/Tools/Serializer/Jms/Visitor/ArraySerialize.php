<?php

namespace ActivityPhp\Tools\Serializer\Jms\Visitor;

use JMS\Serializer\JsonSerializationVisitor;

class ArraySerialize extends JsonSerializationVisitor
{
    public function getResult()
    {
        return $this->getRoot();
    }
}