<?php

namespace ActivityPhp\Tools\Serializer\Jms;

use ActivityPhp\Vocabulary\Object;
use JMS\Serializer\Serializer as JmsSerializer;

final class Jms
{
    private $serializer;

    private $format;

    public function __construct(JmsSerializer $serializer, $format = 'array')
    {
        $this->serializer = $serializer;
        $this->format = $format;
    }

    public function serialize(Object $event)
    {
        return $this->serializer->serialize($event, $this->format);
    }

    public function unserialize($event, $class = 'Knp\Event\Event\Generic')
    {
        return $this->serializer->deserialize($event, $class, $this->format);
    }
}