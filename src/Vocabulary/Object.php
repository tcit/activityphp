<?php

namespace ActivityPhp\Vocabulary;

use ActivityPhp\Vocabulary\Document\Image;
use JMS\Serializer\Annotation as Serializer;
use PhpCollection\Map;
use PhpCollection\Sequence;
use Symfony\Component\Validator\Constraints as Assert;

class Object
{
    /**
     * Provides the globally unique identifier for an Object or Link.
     *
     * @var string
     * @Serializer\Type("string")
     */
    protected $id;

    /**
     * Identifies the Object or Link type. Multiple values may be specified.
     *
     * @var string|Sequence
     * @Serializer\Type("string")
     */
    protected $type;

    /**
     * @var Object|Link|Sequence
     */
    protected $attachment;

    /**
     * @var Object|Link|Sequence
     */
    protected $attributedTo;

    /**
     * @var Object|Link
     */
    protected $audience;

    /**
     * @var string|Map
     * @Serializer\Type("string")
     */
    protected $content;

    /**
     * @var Object|Link
     */
    protected $context;

    /**
     * @var string|Map
     * @Serializer\Type("string")
     */
    protected $name;

    /**
     * @var \DateTime
     *
     * @Assert\DateTime()
     */
    protected $endTime;

    /**
     * @var Object|Link
     */
    protected $generator;

    /**
     * @var Image|Link
     */
    protected $icon;

    /**
     * @var Image
     */
    protected $image;

    /**
     * @var Object|Link
     */
    protected $inReplyTo;

    /**
     * @var Object|Link
     */
    protected $location;

    /**
     * @var Object|Link
     */
    protected $preview;

    /**
     * @var \DateTime
     *
     * @Assert\DateTime()
     * @Serializer\Type("DateTime")
     */
    protected $published;

    /**
     * @var Sequence
     */
    protected $replies;

    /**
     * @var \DateTime
     *
     * @Assert\DateTime()
     */
    protected $startTime;

    /**
     * @var string|Map
     */
    protected $summary;

    /**
     * @var Object|Link|Sequence
     */
    protected $tag;

    /**
     * @var \DateTime
     *
     * @Assert\DateTime()
     */
    protected $updated;

    /**
     * @var string|Link
     *
     * @Assert\Url()
     */
    protected $url;

    /**
     * @var Object|Link|Sequence
     */
    protected $to;

    /**
     * @var Object|Link|Sequence
     */
    protected $bto;

    /**
     * @var Object|Link|Sequence
     */
    protected $cc;

    /**
     * @var Object|Link|Sequence
     */
    protected $bcc;

    /**
     * @var Object|Link
     */
    protected $mediaType;

    /**
     * @var \DateInterval
     */
    protected $duration;

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @param string $id
     * @return Object
     */
    public function setId(string $id): Object
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return Sequence|string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param Sequence|string $type
     * @return Object
     */
    public function setType($type): Object
    {
        $this->type = $type;
        return $this;
    }

    /**
     * @return Link|Object|Sequence
     */
    public function getAttachment()
    {
        return $this->attachment;
    }

    /**
     * @param Link|Object|Sequence $attachment
     * @return Object
     */
    public function setAttachment($attachment): Object
    {
        $this->attachment = $attachment;
        return $this;
    }

    /**
     * @return Link|Object|Sequence
     */
    public function getAttributedTo()
    {
        return $this->attributedTo;
    }

    /**
     * @param Link|Object|Sequence $attributedTo
     * @return Object
     */
    public function setAttributedTo($attributedTo): Object
    {
        $this->attributedTo = $attributedTo;
        return $this;
    }

    /**
     * @return Link|Object
     */
    public function getAudience()
    {
        return $this->audience;
    }

    /**
     * @param Link|Object $audience
     * @return Object
     */
    public function setAudience($audience): Object
    {
        $this->audience = $audience;
        return $this;
    }

    /**
     * @return Map|string
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * @param Map|string $content
     * @return Object
     */
    public function setContent($content): Object
    {
        $this->content = $content;
        return $this;
    }

    /**
     * @return Link|Object
     */
    public function getContext()
    {
        return $this->context;
    }

    /**
     * @param Link|Object $context
     * @return Object
     */
    public function setContext($context): Object
    {
        $this->context = $context;
        return $this;
    }

    /**
     * @return Map|string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param Map|string $name
     * @return Object
     */
    public function setName($name): Object
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getEndTime(): \DateTime
    {
        return $this->endTime;
    }

    /**
     * @param \DateTime $endTime
     * @return Object
     */
    public function setEndTime(\DateTime $endTime): Object
    {
        $this->endTime = $endTime;
        return $this;
    }

    /**
     * @return Link|Object
     */
    public function getGenerator()
    {
        return $this->generator;
    }

    /**
     * @param Link|Object $generator
     * @return Object
     */
    public function setGenerator($generator): Object
    {
        $this->generator = $generator;
        return $this;
    }

    /**
     * @return Image|Link
     */
    public function getIcon()
    {
        return $this->icon;
    }

    /**
     * @param Image|Link $icon
     * @return Object
     */
    public function setIcon($icon): Object
    {
        $this->icon = $icon;
        return $this;
    }

    /**
     * @return Image
     */
    public function getImage(): Image
    {
        return $this->image;
    }

    /**
     * @param Image $image
     * @return Object
     */
    public function setImage(Image $image): Object
    {
        $this->image = $image;
        return $this;
    }

    /**
     * @return Link|Object
     */
    public function getInReplyTo()
    {
        return $this->inReplyTo;
    }

    /**
     * @param Link|Object $inReplyTo
     * @return Object
     */
    public function setInReplyTo($inReplyTo): Object
    {
        $this->inReplyTo = $inReplyTo;
        return $this;
    }

    /**
     * @return Link|Object
     */
    public function getLocation()
    {
        return $this->location;
    }

    /**
     * @param Link|Object $location
     * @return Object
     */
    public function setLocation($location): Object
    {
        $this->location = $location;
        return $this;
    }

    /**
     * @return Link|Object
     */
    public function getPreview()
    {
        return $this->preview;
    }

    /**
     * @param Link|Object $preview
     * @return Object
     */
    public function setPreview($preview): Object
    {
        $this->preview = $preview;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getPublished(): \DateTime
    {
        return $this->published;
    }

    /**
     * @param \DateTime $published
     * @return Object
     */
    public function setPublished(\DateTime $published): Object
    {
        $this->published = $published;
        return $this;
    }

    /**
     * @return Sequence
     */
    public function getReplies(): Sequence
    {
        return $this->replies;
    }

    /**
     * @param Sequence $replies
     * @return Object
     */
    public function setReplies(Sequence $replies): Object
    {
        $this->replies = $replies;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getStartTime(): \DateTime
    {
        return $this->startTime;
    }

    /**
     * @param \DateTime $startTime
     * @return Object
     */
    public function setStartTime(\DateTime $startTime): Object
    {
        $this->startTime = $startTime;
        return $this;
    }

    /**
     * @return Map|string
     */
    public function getSummary()
    {
        return $this->summary;
    }

    /**
     * @param Map|string $summary
     * @return Object
     */
    public function setSummary($summary): Object
    {
        $this->summary = $summary;
        return $this;
    }

    /**
     * @return Link|Object|Sequence
     */
    public function getTag()
    {
        return $this->tag;
    }

    /**
     * @param Link|Object|Sequence $tag
     * @return Object
     */
    public function setTag($tag): Object
    {
        $this->tag = $tag;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getUpdated(): \DateTime
    {
        return $this->updated;
    }

    /**
     * @param \DateTime $updated
     * @return Object
     */
    public function setUpdated(\DateTime $updated): Object
    {
        $this->updated = $updated;
        return $this;
    }

    /**
     * @return Link|string
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * @param Link|string $url
     * @return Object
     */
    public function setUrl($url): Object
    {
        $this->url = $url;
        return $this;
    }

    /**
     * @return Link|Object|Sequence
     */
    public function getTo()
    {
        return $this->to;
    }

    /**
     * @param Link|Object|Sequence $to
     * @return Object
     */
    public function setTo($to): Object
    {
        $this->to = $to;
        return $this;
    }

    /**
     * @return Link|Object|Sequence
     */
    public function getBto()
    {
        return $this->bto;
    }

    /**
     * @param Link|Object|Sequence $bto
     * @return Object
     */
    public function setBto($bto): Object
    {
        $this->bto = $bto;
        return $this;
    }

    /**
     * @return Link|Object|Sequence
     */
    public function getCc()
    {
        return $this->cc;
    }

    /**
     * @param Link|Object|Sequence $cc
     * @return Object
     */
    public function setCc($cc): Object
    {
        $this->cc = $cc;
        return $this;
    }

    /**
     * @return Link|Object|Sequence
     */
    public function getBcc()
    {
        return $this->bcc;
    }

    /**
     * @param Link|Object|Sequence $bcc
     * @return Object
     */
    public function setBcc($bcc): Object
    {
        $this->bcc = $bcc;
        return $this;
    }

    /**
     * @return Link|Object
     */
    public function getMediaType()
    {
        return $this->mediaType;
    }

    /**
     * @param Link|Object $mediaType
     * @return Object
     */
    public function setMediaType($mediaType): Object
    {
        $this->mediaType = $mediaType;
        return $this;
    }

    /**
     * @return \DateInterval
     */
    public function getDuration(): \DateInterval
    {
        return $this->duration;
    }

    /**
     * @param \DateInterval $duration
     * @return Object
     */
    public function setDuration(\DateInterval $duration): Object
    {
        $this->duration = $duration;
        return $this;
    }


}