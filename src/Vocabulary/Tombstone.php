<?php

namespace ActivityPhp\Vocabulary;

class Tombstone extends Object
{
    /**
     * @var Object
     */
    private $formerType;

    /**
     * @var \DateTime
     */
    private $deleted;
}