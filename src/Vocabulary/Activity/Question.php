<?php

namespace ActivityPhp\Vocabulary\Activity;

use ActivityPhp\Vocabulary\Activity;
use ActivityPhp\Vocabulary\Link;
use ActivityPhp\Vocabulary\Object;

class Question extends Activity
{
    /**
     * @var Object|Link
     */
    private $oneOf;

    /**
     * @var Object|Link
     */
    private $anyOf;

    /**
     * @var Object|Link|\DateTime|boolean
     */
    private $closed;
}