<?php

namespace ActivityPhp\Vocabulary;

use Symfony\Component\Validator\Constraints as Assert;

class Place extends Object
{
    /**
     * @var float
     *
     * @Assert\Range(min="0", max="100")
     */
    protected $accuracy;

    /**
     * @var float
     *
     *
     */
    protected $altitude;

    /**
     * @var float
     */
    protected $latitude;

    /**
     * @var float
     */
    protected $longitude;

    /**
     * @var float
     *
     * @Assert\Range(min="0")
     */
    protected $radius;

    /**
     * @var string
     *
     * Assert one of "cm" | " feet" | " inches" | " km" | " m" | " miles" or URI
     */
    protected $units;
}