<?php
/**
 * Created by IntelliJ IDEA.
 * User: tcit
 * Date: 11/09/17
 * Time: 18:37
 */

namespace ActivityPhp\Vocabulary;

use PhpCollection\Map;
use PhpCollection\Sequence;
use Symfony\Component\Validator\Constraints as Assert;

class Link
{
    /**
     * @var string
     *
     * TODO: Validate URI
     */
    protected $href;

    /**
     * @var string|Sequence<string>
     *
     * @Assert\Choice({'alternate', 'author', 'bookmark', 'canonical', 'help', 'icon', 'licence', 'next', 'nofollow', 'noreferrer', 'prefetch', 'prev', 'search', 'stylesheet', 'tag'})
     * TODO: Check for more
     */
    protected $rel;

    /**
     * @var string
     *
     * TODO: Validate against MIME type list
     */
    protected $mediaType;

    /**
     * @var string|Map
     */
    protected $name;

    /**
     * @var string
     *
     * @Assert\Language()
     * TODO: Check that this assertion is BCP47 compatible
     */
    protected $hreflang;

    /**
     * @var int
     *
     * @Assert\Range(min=0)
     */
    protected $height;

    /**
     * @var int
     *
     * @Assert\Range(min=0)
     */
    protected $width;

    /**
     * @var Link|Object|Sequence
     */
    protected $preview;

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->href;
    }
}