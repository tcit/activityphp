<?php

namespace ActivityPhp\Vocabulary\Collection;

use ActivityPhp\Vocabulary\Collection;
use ActivityPhp\Vocabulary\Link;

class CollectionPage extends Collection
{
    /**
     * @var Collection|Link
     */
    private $partOf;

    /**
     * @var CollectionPage|Link
     */
    private $next;

    /**
     * @var CollectionPage|Link
     */
    private $prev;
}