<?php

namespace ActivityPhp\Vocabulary\Actor;

use ActivityPhp\Vocabulary\Actor;

/**
 * Describes a software application.
 */
class Application extends Actor
{

}