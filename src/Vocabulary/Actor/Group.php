<?php

namespace ActivityPhp\Vocabulary\Actor;

use ActivityPhp\Vocabulary\Actor;

/**
 * Represents a formal or informal collective of Actors.
 */
class Group extends Actor
{

}