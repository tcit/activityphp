<?php

namespace ActivityPhp\Vocabulary\Actor;

use ActivityPhp\Vocabulary\Actor;

/**
 * Represents an individual person.
 */
class Person extends Actor
{

}