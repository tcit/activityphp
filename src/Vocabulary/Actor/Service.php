<?php

namespace ActivityPhp\Vocabulary\Actor;

use ActivityPhp\Vocabulary\Actor;

/**
 * Represents a service of any kind.
 */
class Service extends Actor
{

}