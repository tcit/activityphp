<?php

namespace ActivityPhp\Vocabulary;

class Attachment extends Object
{
    /**
     * @var string
     */
    protected $content;

    /**
     * @var string
     */
    protected $url;
}