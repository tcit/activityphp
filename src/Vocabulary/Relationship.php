<?php

namespace ActivityPhp\Vocabulary;

class Relationship extends Object
{
    /**
     * @var Object|Link
     */
    private $subject;

    /**
     * @var Object
     */
    private $object;

    /**
     * @var string
     * TODO: Test URI starts with http://purl.org/vocab/relationship/
     */
    private $relationship;
}