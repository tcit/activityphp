<?php

namespace ActivityPhp\Vocabulary;

use ActivityPhp\Vocabulary\Collection\CollectionPage;
use PhpCollection\Sequence;
use Symfony\Component\Validator\Constraints as Assert;

class Collection extends Object
{
    /**
     * @var int
     *
     * @Assert\Range(min=0)
     */
    private $totalItems;

    /**
     * @var CollectionPage|Link
     */
    private $current;

    /**
     * @var CollectionPage|Link
     */
    private $first;

    /**
     * @var CollectionPage|Link
     */
    private $last;

    /**
     * @var Object|Link|array
     */
    private $items;

    /**
     * @var Sequence
     */
    private $orderedItems;
}