<?php

namespace ActivityPhp\Vocabulary;

use PhpCollection\Sequence;
use JMS\Serializer\Annotation as Serializer;

class Activity extends Object
{
    /**
     * @var Actor|Sequence
     * @Serializer\Type("ActivityPhp\Vocabulary\Actor")
     */
    protected $actor;

    /**
     * @var Object|Sequence
     * @Serializer\Type("ActivityPhp\Vocabulary\Object")
     */
    protected $object;

    /**
     * @var Object|Link
     */
    protected $target;

    /**
     * @var Object|Link
     */
    protected $result;

    /**
     * @var Object|Link
     */
    protected $origin;

    /**
     * @var Object|Link
     */
    protected $instrument;

    /**
     * @return Actor|Sequence
     */
    public function getActor()
    {
        return $this->actor;
    }

    /**
     * @param Actor|Sequence $actor
     * @return Activity
     */
    public function setActor($actor): Activity
    {
        $this->actor = $actor;
        return $this;
    }

    /**
     * @return Object|Sequence
     */
    public function getObject()
    {
        return $this->object;
    }

    /**
     * @param Object|Sequence $object
     * @return Activity
     */
    public function setObject($object): Activity
    {
        $this->object = $object;
        return $this;
    }

    /**
     * @return Link|Object
     */
    public function getTarget()
    {
        return $this->target;
    }

    /**
     * @param Link|Object $target
     * @return Activity
     */
    public function setTarget($target): Activity
    {
        $this->target = $target;
        return $this;
    }

    /**
     * @return Link|Object
     */
    public function getResult()
    {
        return $this->result;
    }

    /**
     * @param Link|Object $result
     * @return Activity
     */
    public function setResult($result): Activity
    {
        $this->result = $result;
        return $this;
    }

    /**
     * @return Link|Object
     */
    public function getOrigin()
    {
        return $this->origin;
    }

    /**
     * @param Link|Object $origin
     * @return Activity
     */
    public function setOrigin($origin): Activity
    {
        $this->origin = $origin;
        return $this;
    }

    /**
     * @return Link|Object
     */
    public function getInstrument()
    {
        return $this->instrument;
    }

    /**
     * @param Link|Object $instrument
     * @return Activity
     */
    public function setInstrument($instrument): Activity
    {
        $this->instrument = $instrument;
        return $this;
    }

}