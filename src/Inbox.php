<?php


namespace ActivityPhp;


use ActivityPhp\Vocabulary\Activity;
use ActivityPhp\Vocabulary\Object;
use Doctrine\Common\Annotations\AnnotationRegistry;
use JMS\Serializer\SerializerBuilder;

class Inbox
{
    public function post(string $data): Object
    {
        AnnotationRegistry::registerLoader('class_exists');

        $serializer = SerializerBuilder::create()->build();
        return $serializer->deserialize($data, Activity::class,'json');
    }
}